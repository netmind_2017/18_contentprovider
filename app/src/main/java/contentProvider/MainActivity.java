package contentProvider;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.provider.ContactsContract;
import android.widget.TextView;

import contentProvider.R;


public class MainActivity extends Activity {
	public TextView outputText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		outputText = (TextView) findViewById(R.id.textView1);
		fetchContacts();
	}

	public void fetchContacts() {
		
		String phoneNumber = null;
		
		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
		
		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		
		String output = new String();
		
		ContentResolver contentResolver = getContentResolver();
		
		Cursor cursor = contentResolver.query(CONTENT_URI, null,null, null, null);	
		

		if (cursor.getCount() > 0) {
			
			while (cursor.moveToNext()) {
				
				String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
				String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));

				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));
				
				if (hasPhoneNumber > 0) {
					
					output+=("\n First Name:" + name);
					

					Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
					
					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
						output+=("\n Phone number:" + phoneNumber);
						
					
					}
					
					phoneCursor.close();


				}

				output+=("\n");
			}

			outputText.setText(output);
		}
	}

}